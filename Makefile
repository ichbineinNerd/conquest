SHELL=/bin/bash
CXX=g++
CXXFLAGS=-Wall -std=c++11 # -Werror
# Flags for the RELEASE branch (no debugging symbols)
REL_DYN=-O2
REL_STATIC=-O2 -static
# Flags for the DEBUG branch (debugging symbols active)
DEBUG_DYN=-g -O0
DEBUG_STC=-g -O0 -static
# Flags for the OPTIMIZE_SIZE branch
SIZE_DYN=-Os
SIZE_STC=-Os -static

# Library flags from allegro 5
#LIBS=-lallegro_primitives -lallegro_main -lallegro_acodec -lallegro_dialog -lallegro_ttf -lallegro_audio -lallegro_font -lallegro_image -lallegro
#STATIC_LIBS=-I/usr/local/include -L/usr/local/lib -lallegro_primitives-static -lallegro_main-static -lallegro_acodec-static -lFLAC -logg -lvorbisfile -lvorbis -logg -lallegro_dialog-static -lgtk-x11-2.0 -lgdk-x11-2.0 -lpangocairo-1.0 -latk-1.0 -lcairo -lgdk_pixbuf-2.0 -lgio-2.0 -lpangoft2-1.0 -lpango-1.0 -lgobject-2.0 -lglib-2.0 -lfontconfig -lfreetype -lgthread-2.0 -lglib-2.0 -lallegro_ttf-static -lfreetype -lallegro_audio-static -lpulse-simple -lpulse -lasound -lopenal -lallegro_font-static -lallegro_image-static -lpng -lz -ljpeg -lwebp -lallegro-static -lm -lpthread -lSM -lICE -lX11 -lXext -lXcursor -lXpm -lXi -lXinerama -lXrandr -lGLU -lGL
LIBS=-L/usr/local/lib -lSDL2 -lSDL2_image -lSDL2_mixer -lSDL2_ttf 
#LIBS=-L./dynamic-libs -lSDL2 -lSDL2_image -lSDL2_mixer -lSDL2_ttf 
# -I/opt/SDL,/opt/SDL2_image-2.0.3,/opt/SDL2_mixer-2.0.2,/opt/SDL2_ttf-2.0.14
# -D_REENTRANT -I/usr/local/include/SDL2 -L/usr/local/lib -lSDL2_image -lSDL2_mixer -lSDL2_ttf -Wl,-rpath,/usr/local/lib -Wl,--enable-new-dtags -lSDL2
STATIC_LIBS=-D_REENTRANT -I/usr/local/include/SDL2 -L/usr/local/lib -Wl,-rpath,/usr/local/lib -Wl,--enable-new-dtags -lSDL2 -lSDL2_image -lSDL2_ttf -lSDL2_mixer -Wl,--no-undefined -lm -ldl -lpthread -lrt
SAVED_DATE=`/bin/date +%F`
EXEC=game-loop # oo-tmb
SHARED_DIR=dynamic-libs/

# The following line is very specific to my machine. DEVELOPERS - Please change it when running on your machine:
# In the future - also have it saved to an external drive  for security.
BACKUP_DIR=/home/chocorho/git/chocorho/allegro-game-release

#execs: $(EXEC) # major executable files, then make clean
#all: game-loop oo-tmb set-config clean release backups
all: directories game-loop # init oo-tmb
#objects: game-loop oo-tmb set-config release backups# NOTE: NO `clean` !

.PHONY: clean init
clean:
	@echo "Cleaning up all object files..."
	@rm -f *.o bin/*.o
	@echo "Finished cleaning."

game-loop: SDL_FontCache.o animation.o tmb.o game-loop.o Game.cpp.o game-loop.o
	${CXX} -o bin/dynamic/release/game-loop obj/dynamic/release/SDL_FontCache.o obj/dynamic/release/Game.cpp.o obj/dynamic/release/animation.o obj/dynamic/release/tmb.cpp.o obj/dynamic/release/game-loop-rel.cpp.o -L${SHARED_DIR} ${LIBS} # -Wl,-rpath,for-static-release-dynamic-libs/ # no pie
	${CXX} -o bin/dynamic/debug/game-loop obj/dynamic/debug/SDL_FontCache.o obj/dynamic/debug/Game.cpp.o obj/dynamic/debug/animation.o obj/dynamic/debug/tmb.cpp.o obj/dynamic/debug/game-loop-debug.cpp.o -L${SHARED_DIR} ${LIBS} # -Wl,-rpath,for-static-release-dynamic-libs/ # no pie
	${CXX} -o bin/dynamic/opt_size/game-loop obj/dynamic/release/SDL_FontCache.o obj/dynamic/opt_size/Game.cpp.o obj/dynamic/opt_size/animation.o obj/dynamic/opt_size/tmb.cpp.o obj/dynamic/opt_size/game-loop.cpp.o -L${SHARED_DIR} ${LIBS} # -Wl,-rpath,for-static-release-dynamic-libs/ # no pie
	${CXX} -o bin/static/release/game-loop obj/static/release/SDL_FontCache.o obj/static/release/animation.o obj/static/release/tmb.cpp.o obj/static/release/Game.cpp.o obj/static/release/game-loop.cpp.o ${STATIC_LIBS} # -Wl,-rpath,for-static-release-dynamic-libs/
	${CXX} -o bin/static/debug/game-loop obj/static/debug/SDL_FontCache.o obj/static/debug/animation.o obj/static/debug/tmb.cpp.o obj/static/debug/Game.cpp.o obj/static/debug/game-loop.cpp.o ${STATIC_LIBS} # -Wl,-rpath,for-static-release-dynamic-libs/
	${CXX} -o bin/static/opt_size/game-loop obj/static/release/SDL_FontCache.o obj/static/opt_size/animation.o obj/static/opt_size/tmb.cpp.o obj/static/opt_size/Game.cpp.o obj/static/opt_size/game-loop.cpp.o ${STATIC_LIBS} # -Wl,-rpath,for-static-release-dynamic-libs/
	${CXX} -o bin/libstd-static/release/game-loop obj/static/release/SDL_FontCache.o obj/static/release/animation.o obj/static/release/tmb.cpp.o obj/static/release/Game.cpp.o obj/static/release/game-loop.cpp.o ${STATIC_LIBS} # -static-libstdc++
	${CXX} -o bin/libstd-static/debug/game-loop obj/static/debug/SDL_FontCache.o obj/static/debug/animation.o obj/static/debug/tmb.cpp.o obj/static/debug/Game.cpp.o obj/static/debug/game-loop.cpp.o ${STATIC_LIBS} # -static-libstdc++
	${CXX} -o bin/libstd-static/opt_size/game-loop obj/static/release/SDL_FontCache.o obj/static/opt_size/animation.o obj/static/opt_size/tmb.cpp.o obj/static/opt_size/Game.cpp.o obj/static/opt_size/game-loop.cpp.o ${STATIC_LIBS} # -static-libstdc++
#	${CXX} -o bin/dyn-pie/release/game-loop obj/dynamic/release/game-loop-rel.cpp.o -L${SHARED_DIR} -pie -fPIE ${LIBS}
#	${CXX} -o bin/dyn-pie/debug/game-loop obj/dynamic/debug/game-loop-debug.cpp.o -L$(SHARED_DIR) -pie -fPIE ${LIBS}
#	${CXX} -o bin/dyn-pie/opt_size/game-loop obj/dynamic/opt_size/game-loop.cpp.o -L$(SHARED_DIR) -pie -fPIE ${LIBS}

SDL_FontCache.o: src/SDL_FontCache.hpp # Compile it here? Or TODO make a static library?
	${CXX} -c -o obj/dyn-pie/release/SDL_FontCache.o src/SDL_FontCache.cpp ${CXXFLAGS} ${REL_DYN} -pie -fPIE
	${CXX} -c -o obj/dyn-pie/debug/SDL_FontCache.o src/SDL_FontCache.cpp ${CXXFLAGS} ${REL_DEBUG} -pie -fPIE
	${CXX} -c -o obj/dynamic/release/SDL_FontCache.o src/SDL_FontCache.cpp ${CXXFLAGS} ${REL_DYN}
	${CXX} -c -o obj/dynamic/debug/SDL_FontCache.o src/SDL_FontCache.cpp ${CXXFLAGS} ${DEBUG_DYN}
	${CXX} -c -o obj/static/release/SDL_FontCache.o src/SDL_FontCache.cpp ${CXXFLAGS} ${REL_STATIC}
	${CXX} -c -o obj/static/debug/SDL_FontCache.o src/SDL_FontCache.cpp ${CXXFLAGS} ${DEBUG_STC}
#	${CXX} -c -o obj/dynamic/release/game-loop-rel.cpp.o src/SDL_FontCache.cpp

animation.o: src/animation.hpp src/animation.cpp
	${CXX} -c -o obj/dynamic/release/animation.o src/animation.cpp ${CXXFLAGS} ${REL_DYN} -pie -fPIE
	${CXX} -c -o obj/dynamic/debug/animation.o src/animation.cpp ${CXXFLAGS} ${DEBUG_DYN} -pie -fPIE
	${CXX} -c -o obj/dynamic/opt_size/animation.o src/animation.cpp ${CXXFLAGS} ${SIZE_DYN} -pie -fPIE
	${CXX} -c -o obj/static/release/animation.o src/animation.cpp ${CXXFLAGS} ${REL_STATIC}
	${CXX} -c -o obj/static/debug/animation.o src/animation.cpp ${CXXFLAGS} ${DEBUG_STC}
	${CXX} -c -o obj/static/opt_size/animation.o src/animation.cpp ${CXXFLAGS} ${SIZE_STC}

tmb.o: src/ai.hpp src/entity.hpp src/global.hpp src/graph.hpp src/item.hpp src/unit.hpp src/weapon.hpp src/SDL_FontCache.hpp src/tmb.cpp
	${CXX} -c -o obj/dynamic/release/tmb.cpp.o src/tmb.cpp ${CXXFLAGS} ${REL_DYN} -pie -fPIE
	${CXX} -c -o obj/dynamic/debug/tmb.cpp.o src/tmb.cpp ${CXXFLAGS} ${DEBUG_DYN} -pie -fPIE
	${CXX} -c -o obj/dynamic/opt_size/tmb.cpp.o src/tmb.cpp ${CXXFLAGS} ${SIZE_DYN} -pie -fPIE
	${CXX} -c -o obj/static/release/tmb.cpp.o src/tmb.cpp ${CXXFLAGS} ${REL_STATIC}
	${CXX} -c -o obj/static/debug/tmb.cpp.o src/tmb.cpp ${CXXFLAGS} ${DEBUG_STC}
	${CXX} -c -o obj/static/opt_size/tmb.cpp.o src/tmb.cpp ${CXXFLAGS} ${SIZE_STC}

Game.cpp.o: src/Game.hpp src/game-loop.hpp src/tmb.hpp src/global.hpp src/unit.hpp src/tmb.hpp src/animation.hpp src/animation.hpp src/entity.hpp src/item.hpp src/weapon.hpp src/SDL_FontCache.hpp src/Game.cpp
	${CXX} -c -o obj/dynamic/release/Game.cpp.o src/Game.cpp ${CXXFLAGS} ${REL_DYN}
	${CXX} -c -o obj/dynamic/debug/Game.cpp.o src/Game.cpp ${CXXFLAGS} ${DEBUG_DYN}
	${CXX} -c -o obj/dynamic/opt_size/Game.cpp.o src/Game.cpp ${CXXFLAGS} ${SIZE_DYN}
	${CXX} -c -o obj/static/release/Game.cpp.o src/Game.cpp ${CXXFLAGS} ${REL_STATIC}
	${CXX} -c -o obj/static/debug/Game.cpp.o src/Game.cpp ${CXXFLAGS} ${DEBUG_STC}
	${CXX} -c -o obj/static/opt_size/Game.cpp.o src/Game.cpp ${CXXFLAGS} ${SIZE_STC}
#	${CXX} -c -o obj/dynamic/release/Game.cpp.o src/Game.cpp ${CXXFLAGS} ${REL_DYN} -pie -fPIE
#	${CXX} -c -o obj/dynamic/debug/Game.cpp.o src/Game.cpp ${CXXFLAGS} ${DEBUG_DYN} -pie -fPIE
#	${CXX} -c -o obj/dynamic/opt_size/Game.cpp.o src/Game.cpp ${CXXFLAGS} ${SIZE_DYN} -pie -fPIE

game-loop.o: src/*.hpp
	${CXX} -c -o obj/dynamic/release/game-loop-rel.cpp.o src/main.cpp ${CXXFLAGS} ${REL_DYN} -pie -fPIE
	${CXX} -c -o obj/dynamic/debug/game-loop-debug.cpp.o src/main.cpp ${CXXFLAGS} ${DEBUG_DYN} -pie -fPIE
	${CXX} -c -o obj/dynamic/opt_size/game-loop.cpp.o src/main.cpp ${CXXFLAGS} ${SIZE_DYN} -pie -fPIE
	${CXX} -c -o obj/dynamic/release/game-loop-rel.cpp.o src/main.cpp ${CXXFLAGS} ${REL_DYN}
	${CXX} -c -o obj/dynamic/debug/game-loop-debug.cpp.o src/main.cpp ${CXXFLAGS} ${DEBUG_DYN}
	${CXX} -c -o obj/dynamic/opt_size/game-loop.cpp.o src/main.cpp ${CXXFLAGS} ${SIZE_DYN}
	${CXX} -c -o obj/static/release/game-loop.cpp.o src/main.cpp ${CXXFLAGS} ${REL_STATIC}
	${CXX} -c -o obj/static/debug/game-loop.cpp.o src/main.cpp ${CXXFLAGS} ${DEBUG_STC}
	${CXX} -c -o obj/static/opt_size/game-loop.cpp.o src/main.cpp ${CXXFLAGS} ${SIZE_STC}
#	${CXX} -c -o obj/dynamic/release/game-loop-rel.cpp.o src/game-loop.cpp ${CXXFLAGS} ${REL_DYN} -pie -fPIE
#	${CXX} -c -o obj/dynamic/debug/game-loop-debug.cpp.o src/game-loop.cpp ${CXXFLAGS} ${DEBUG_DYN} -pie -fPIE
#	${CXX} -c -o obj/dynamic/opt_size/game-loop.cpp.o src/game-loop.cpp ${CXXFLAGS} ${SIZE_DYN} -pie -fPIE
#	${CXX} -c -o obj/dynamic/release/game-loop-rel.cpp.o src/game-loop.cpp ${CXXFLAGS} ${REL_DYN}
#	${CXX} -c -o obj/dynamic/debug/game-loop-debug.cpp.o src/game-loop.cpp ${CXXFLAGS} ${DEBUG_DYN}
#	${CXX} -c -o obj/dynamic/opt_size/game-loop.cpp.o src/game-loop.cpp ${CXXFLAGS} ${SIZE_DYN}
#	${CXX} -c -o obj/static/release/game-loop.cpp.o src/game-loop.cpp ${CXXFLAGS} ${REL_STATIC}
#	${CXX} -c -o obj/static/debug/game-loop.cpp.o src/game-loop.cpp ${CXXFLAGS} ${DEBUG_STC}
#	${CXX} -c -o obj/static/opt_size/game-loop.cpp.o src/game-loop.cpp ${CXXFLAGS} ${SIZE_STC}

directories:
	@mkdir -p bin/{dyn-pie,dynamic,static,libstd-static}/{release,debug,opt_size}
	@mkdir -p obj/{dyn-pie,dynamic,static}/{release,debug,opt_size}


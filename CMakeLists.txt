
cmake_minimum_required(VERSION 3.0)
project(MGG)
set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} "${MGG_SOURCE_DIR}/cmake-find-modules")
add_executable(game-loop src/tmb.cpp  src/SDL_FontCache.cpp src/Game.cpp src/main.cpp src/animation.cpp)

# Set compilers
# Comment the following two lines if CMake build fails:
SET(CMAKE_C_COMPILER /usr/bin/cc)
SET(CMAKE_CXX_COMPILER /usr/bin/c++)
# and, UNCOMMENT the following single line if CMake build fails:
#SET(CMAKE_CXX_COMPILER /usr/local/bin/c++)

set(CMAKE_CXX_STANDARD 11)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

#set(CMAKE_CXX_EXTENSIONS OFF)

find_package(SDL2 REQUIRED)
find_package(SDL2_ttf REQUIRED)
find_package(SDL2_mixer REQUIRED)
find_package(SDL2_image REQUIRED)

include_directories(${SDL2_INCLUDE_DIRS}
        ${SDL2_IMAGE_INCLUDE_DIR}
        ${SDL2_TTF_INCLUDE_DIR}
        ${SDL2_MIXER_INCLUDE_DIR}
        )

target_link_libraries(game-loop ${SDL2_LIBRARY}
        ${SDL2_IMAGE_LIBRARIES}
        ${SDL2_TTF_LIBRARIES}
        ${SDL2_MIXER_LIBRARIES}
        )
#link_directories()
file(COPY res/ DESTINATION ${CMAKE_BINARY_DIR}/res )


#include <iostream>
#include <vector>

#define DEFAULT_SIZE 32

class Edge {
  public:
    Edge(int srx, int sry, int dstx, int dty, int cost) {
      srcX = srx;
      srcY = sry;
      destX = dstx;
      destY = dty;
      weight = cost;
    }

    int getSrcX() const {
      return srcX;
    }

    int getSrcY() const {
      return srcY;
    }

    int getX() const {
      return destX;
    }

    int getY() const {
      return destY;
    }

    int getWeight() const {
      return weight;
    }

    void print() const {
      std::cout<<"("<<srcX<<", "<<srcY<<") === ["<<weight<<"] ===> "<<
        "("<<destX<<", "<<destY<<")"<<std::endl;
    }
  protected:
    int srcX, srcY, destX, destY, weight;
};

/* I know what you're thinking.
 * "Why make a wrapper for an X,Y coordinate AND a distance value?
   "This is a very peculiar structure, isn't it?"
 * Well, yes, it is unusual.
 * The implementation of Djikstra's Algorithm uses a HEAP, and that
 * Heap must keep track of every tile, ALONG WITH a measurement 
 * of that tile's distance from the source (player unit's current location)
 * at the time when the unit is selected.
 * Yes, it's a PITA, but I'm told it's more efficient than
 * that run-of-the-mill Dijkstra's alg, and (hopefully) more so
 * than the primitive recursive approach used in versions 0.0.1-3.
 * 그래서, a Node (used only inside the Heap and nowhere else
 * at the time of this writing) contains *what* exactly?
 * {Vertex (Entity), distance value} = Node in the min Heap. */
class Node {
  public:
    Node(int tx, int ty, int dt) : previous{-1, -1} {
      x = tx;
      y = ty;
      distance = dt;
    }
    Node(int tx, int ty) : previous{-1, -1} {
      x = tx;
      y = ty;
      distance = INFINITE_DIST; 
    }

    /* accessor */
    int getX() const {
      return x;
    }
    int getY() const {
      return y;
    }
    int getDistance() const {
      return distance;
    }
    const Entity &getPrevious() const {
      return previous;
    }
    
    /* Precondition: newDist is some nonnegative value.
     * Postcondition: if the distance is updated,
     *  then it actually updates the distance - and returns true.
     * Returns false otherwise. */
    bool setDistance(int newDist) {
      if ((newDist < distance) || (distance == INFINITE_DIST)) {
        distance = newDist;
        return true;
      }
      return false;
    }
    void setPrevious(Entity &quicker) {
      previous = quicker;
    }
    
    bool operator <(Node other) const {
      return (distance < other.getDistance() ||
        (other.getDistance() == INFINITE_DIST));
    }
    bool operator ==(Node other) const {
      return (x==other.getX() && y==other.getY());
    }
    bool operator !=(Node other) const {
      return (x!=other.getX() || y!=other.getY());
    }
    
    /* Precondition: width is nonnegative  
     * Postcondition: computes a map-dependent hash value but wait.
     * Is this function even necessary? I already defined operators
     * `==` and `<` below. Lol. */
    int hash(int width) const {
      return (width*y + x);
    }

    void print() const {
      std::cout<<"("<<x<<", "<<y<<")"<<" with distance = ["<<distance<<"]"<<std::endl;
    }
    
  protected:
    int x;
    int y;
    int distance;
    Entity previous;
};

namespace std
{
  template <>
  struct hash<Node> {
    size_t operator()(const Node &loc) const {
      /* TODO think of a good way to hash
       * 2d array elements without knowing width
       * a priori.
       * Hmm. This might suck. Maybe come up with a clever
       * XOR scheme that minimizes collisions? */
       return loc.getX()+8*loc.getY();
    }
  };
};

/* TODO somehow require that T obeys some
 * "hash" function requirement,
 * in order to be able to replace elements
 * if they are in fact equal. */
/*template <class T>*/
class Heap {
  public:
    Heap() {
/*      backing_arr = new T[DEFAULT_SIZE];
      size = 0;
      capacity = DEFAULT_SIZE;*/
      backing = new std::vector<Node>(/*DEFAULT_SIZE*/);
    }
    ~Heap() {
/*      delete backing_arr;*/
      delete backing;
    }
    
    void insert(/*T*/Node ele) {
/*      backing_arr[size] = ele;*/
/*      std::cout<<"[ --   Now inserting "<<ele<<" -- ]"<<std::endl;*/
      backing->push_back(ele);
      for (int ind=backing->size()-1; ind > 0; ind --) {
        if ((*backing)/*_arr*/[ind] < (*backing)/*_arr*/[(ind-1)/2]) {
          Node tmp = (*backing)/*_arr*/[(ind-1)/2];
          (*backing)/*_arr*/[(ind-1)/2] = (*backing)/*_arr*/[ind];
          (*backing)/*_arr*/[ind] = tmp;
        }
      }
/*      size++;
      if (2*size > capacity) {
        capacity *= 2;
        backing_arr = (T*)realloc(backing_arr, capacity*sizeof(T));
        std::cerr<<"[-- Raising capacity to "<<capacity<<" --]"<<std::endl;
      }*/
    }

    /*  Precondition: dist is a nonnegative integer, query is non-null
     *   (references can't be nullptr, right? I think? See the rant below.)
     * TODO perhaps replace the TYPE of the first argument
     *  to make it ENTITY rather than NODE...
     *  this would certainly eliminate confusion, because
     *  all the user really wants to do is specify a location --
     *  they will not know the original distance value a priori.*/
    bool replace(Node query, int dist, Entity intermediate) {
      int d0 = -10;
      /* IMPORTANT: Do NOT try to replace this with the 
       * for-each loop that is recommended by the C++
       * Style Guide!!!!
       * We need to keep the INDEX VALUE of the identified Node
       * So that we can properly PERCOLATE upward or downward,
       * keeping track of indices. */
      int ind = -1;
      for (unsigned int iterInd=0; iterInd < backing->size(); iterInd ++) {
        auto &iter = (*backing)[iterInd];
        if (iter.getX() == query.getX() && iter.getY() == query.getY()) {
          d0 = iter.getDistance();
          ind = iterInd;
          iterInd = backing->size();
        }
      }
      if (d0 < (-1)) {
        /* NOT found. Hm. Return and print an error?
         * And, TODO once I learn how exceptions work in C++, maybe
         * throw one o' those. */
        std::cerr<<"[ HMM ]    Node not found in the min Heap! Exiting."<<std::endl;
        return false;
      } else if ((d0 < dist)/* && (d0 != INFINITE_DIST)*/) {
/*        std::cout<<"[ HMM ]    New distance is not any better, lol! Exiting."<<std::endl;*/
        return false;
      }
      (*backing)[ind].setDistance(dist);
      (*backing)[ind].setPrevious(intermediate);

      /* Now decide whether to percolate UPWARD and DOWNWARD */
      if ((*backing)[ind] < (*backing)[(ind-1)/2]) {
        /* Percolate UPWARD */
        while (ind > 0) {
          if ((*backing)/*_arr*/[ind] < (*backing)/*_arr*/[(ind-1)/2]) {
            /* Swap 'em! */
            Node tmp = (*backing)/*_arr*/[(ind-1)/2];
            (*backing)/*_arr*/[(ind-1)/2] = (*backing)/*_arr*/[ind];
            (*backing)/*_arr*/[ind] = tmp;
          }
          ind = (ind-1)/2;
        }
      } else {
        if ((2*ind+2) < backing->size() &&
                   (*backing)[2*ind+2] < (*backing)[2*ind+1]) {
          /* Percolate DOWNWARD (right) */
          unsigned int minInd = 2*ind+2;
          while ((minInd < backing->size()) &&
              ((*backing)[minInd] < (*backing)[ind])) {
            /* Perform the actual swap */
            Node tmp = (*backing)[minInd];
            (*backing)[minInd] = (*backing)[ind];
            (*backing)[ind] = tmp;
  
            /* Now update index variables */
            ind = minInd;
            minInd = ((*backing)[2*minInd+2] < (*backing)[2*minInd+1])
                ? (2*minInd+2): (2*minInd+1);
          }
        } else if ((2*ind+1) < backing->size() &&
                   (*backing)[2*ind+1] < (*backing)[ind]) {
          /* Percolate DOWNWARD (left) */
          unsigned int minInd = 2*ind+1;
          while ((minInd < backing->size()) &&
              ((*backing)[minInd] < (*backing)[ind])) {
            /* Perform the actual swap */
            Node tmp = (*backing)[minInd];
            (*backing)[minInd] = (*backing)[ind];
            (*backing)[ind] = tmp;
  
            /* Now update index variables */
            ind = minInd;
            minInd = ((*backing)[2*minInd+2] < (*backing)[2*minInd+1])
                ? (2*minInd+2): (2*minInd+1);
          }
        }
      }
      return true;
    }

    /* A question, to anyone who REALLY understands modern C++:
     * Can you implement this function with a return type of T,
     * rather than a return type of (T*)?
     * You probably *can* do so using "move constructors" or some similar
     * mystical sh*t that they never taught me in school
     * (so much for becoming a comp sci major).
     * You'd need to "move" the root element into a NEW (separate)
     * memory location, and THEN overwrite the root of the tree. The 
     * "NEW (separate)" location saves the original root's data. 
     * Also, apparently you CAN write `T result = nullptr;` and it compiles
     * just fine.
     * Yea, your guess is as good as mine. I was taught references could
     * not be NULL, but I guess that magically changes in C++11. 
     * Also I was taught by the C experts never to return a pointer
     * to a local variable. But I'm getting used to that no longer being
     * the case in C++.
     * On the other hand, I guess this means that C++ is closer to Java
     * than I expected. So that's nifty. 
     * Anyway. Back to this dumb project. 
     * cho. */
    /*T*/Node extractMin() {
//      /*T*/Node result/* = nullptr*/;
//      std::cout<<std::endl;
      if (backing->size() > 1) {
        Node result = backing/*_arr*/->front();
        /*T*/Node newRoot = backing->back()/*_arr[size-1]*/;
        (*backing)[0] = newRoot;
        backing->pop_back();
        for (unsigned int ind=1; ind < backing->size(); ind++) {
          unsigned int sibl = /*(ind % 2 == 0) ? sibl=ind-1: sibl=*/ind+1;
//          std::cout<<"Checking ["<<ind<<"] and its sibling ["<<sibl<<"]"<<std::endl;
          if (sibl < backing->size()) {
            if ((*backing)/*_arr*/[ind] < (*backing)/*_arr*/[sibl]) {
              if ((*backing)/*_arr*/[ind] < (*backing)/*_arr*/[(ind-1)/2]) {
                /* Swap 'em! */
//                std::cout<<" [- debug -]     swapping "<<(*backing)[ind]<<" and "<<(*backing)[(ind-1)/2]<<std::endl;
                /*T*/Node tmp = /*T*/((*backing)/*_arr*/[ind]);
                (*backing)/*_arr*/[ind] = (*backing)/*_arr*/[(ind-1)/2];
                (*backing)/*_arr*/[(ind-1)/2] = tmp;
                ind <<= 1;
              } else {
                ind = backing->size();
              }
            } else { /* sibling node is smaller value */
              if ((*backing)/*_arr*/[sibl] < (*backing)/*_arr*/[(ind-1)/2]) {
                /* Swap 'em! */
//                std::cout<<" [- debug -]     swapping "<<(*backing)[sibl]<<" and "<<(*backing)[(ind-1)/2]<<std::endl;
                /*T*/Node tmp = /*T*/((*backing)/*_arr*/[sibl]);
                (*backing)/*_arr*/[sibl] = (*backing)/*_arr*/[(ind-1)/2];
                (*backing)/*_arr*/[(ind-1)/2] = tmp;
                ind = (sibl << 1);
              } else {
                ind = backing->size();
              }
            }
          } else {
            /* Simply compare the value at ind to its parent and be done. */
            if ((*backing)/*_arr*/[ind] < (*backing)/*_arr*/[(ind-1)/2]) {
                /* Swap 'em! */
                /*T*/Node tmp = /*T*/((*backing)/*_arr*/[ind]);
                (*backing)/*_arr*/[ind] = (*backing)/*_arr*/[(ind-1)/2];
                (*backing)/*_arr*/[(ind-1)/2] = tmp;
                ind <<= 1;
            }
          }
        }
        return result;
      } else if (backing->size() == 1) {
        Node copiedIHope = backing->front();
        backing/*_arr*/->pop_back();
        return copiedIHope;
      } else {
        /* Since I can't return a nullptr if the return type is Node,
         * what should this be? Some arbitrary sentinel value?
         * Or should I just keep it simple and make the return type
         * (Node *)? God forbid we keep things simple and use
         * naked pointers. Nope, that's not "[making] use of language 
         * features [correctly]," as a special someone would say. */
        return Node(-1, -1, -1);
      }
      /*if (4*size < capacity) {
        capacity /= 2;
        backing_arr = (T*)realloc(backing_arr, capacity*sizeof(T));
        std::cerr<<"[-- Lowering capacity to "<<capacity<<" --]"<<std::endl;
      }*/
    }

    bool contains(Node &query) const {
      for (auto& iter : (*backing)) {
        if (iter.getX() == query.getX() && iter.getY() == query.getY()) {
          return true;
        }
      }
      return false;
    }

    int getDist(Node query) const {
      for (auto& iter : (*backing)) {
        if (iter.getX() == query.getX() && iter.getY() == query.getY()) {
          return iter.getDistance();
        }
      }
      return -10;
    }
    
    bool isEmpty() const {
      return backing->empty();
    }
    
    /* TODO add an iostream parameter */
    void print() const {
      std::cout<<std::endl<<"[Heap looks like:]"<<std::endl;
      for (unsigned int ind=0; ind < backing->size(); ind ++/*T *iter : backing_arr*/) {
        std::cout<<" [ind "<<ind<<"] ";
        (*backing)/*_arr*/[ind]/*(*iter)*/.print();
      }
    }
  
  protected:
    /* TODO Consider replacing this with a std::vector?
     * I guess vector already has a size and a capacity
     * that works much the same way... */
    /* You know what, I guess I'll try both and check the
     * differences in performance, when run against
     * the same input */
    /* TODO check performance delta */
    /*    T* backing_arr=nullptr;
     int size;
     int capacity;*/
    std::vector<Node> *backing;
};

#ifdef _MSC_VER
  #define _CRT_SECURE_NO_WARNINGS
#endif
#include "animation.hpp"

/* Sprite Frame (list node) implementation */
//Frame::Frame(SDL_Texture *sprite) {
//  m_sprite = sprite;
//  m_surf = nullptr;
//}
// This Constructor is not actually used, but could be someday.
//Frame::Frame(SDL_Renderer *renderer, SDL_Surface *g) {
//  m_surf = g;
//  m_sprite = SDL_CreateTextureFromSurface(renderer, g);
//}

Frame::Frame(/*SDL_Renderer *renderer, */const std::string &sprite) {
  SDL_RWops *sprite_ops = SDL_RWFromFile(sprite.c_str(), "rb");
  m_surf = IMG_LoadPNG_RW(sprite_ops);
  m_sprite = nullptr;
}

/* Headers from the .hpp file, for reference:
     Frame(SDL_Surface *sprite);
     Frame(const std::string &sprite);*/

Frame::~Frame() {
  if (m_sprite != nullptr) {
    SDL_DestroyTexture(m_sprite);
  } else {
    
  }
  if (m_surf != nullptr) {
    SDL_FreeSurface(m_surf);
  }
}

void Frame::setNextFrame(Frame *next) {
  m_next = next;
}

void Frame::actualize(SDL_Renderer *renderer) {
  m_sprite = SDL_CreateTextureFromSurface(renderer, m_surf);
}

void Frame::deActualize() {
  SDL_DestroyTexture(m_sprite);
}

Frame *Frame::getNextFrame() const {
  return m_next;
}

SDL_Texture *Frame::getSprite() {
  return m_sprite;
}

/* CircularList implementation */

CircularList::CircularList() {
  head = nullptr;
}

CircularList::~CircularList() {
  if (head == nullptr) {
    return;
  } else if (head->getNextFrame() == nullptr) {
    delete head;
    return;
  }
  deleteAllFrames(head->getNextFrame());
  delete head;
}

//void CircularList::addFrame(SDL_Texture *s) {
//  if (head == nullptr) {
//    head = new Frame(s);
//    head->setNextFrame(head);
//    return;
//  }
//  Frame *saved = head;
//  Frame *cur = head;
//  Frame *next = head->getNextFrame();
//  while (/*next != nullptr && */next != saved) {
//    cur = next;
//    next = next->getNextFrame();
//  }
//  /* Allocate the new memory and redirect the two "next pointers" */
//  Frame *added = new Frame(s);
//  added->setNextFrame(saved);
//  cur->setNextFrame(added);
//}

void CircularList::addFrame(/*SDL_Renderer *r, */const std::string &s) {
  if (head == nullptr) {
    head = new Frame(/*r, */s);
    head->setNextFrame(head);
    return;
  }
  Frame *saved = head;
  Frame *cur = head;
  Frame *next = head->getNextFrame();
  while (/*next != nullptr && */next != saved) {
    cur = next;
    next = next->getNextFrame();
  }
  /* Allocate the new memory and redirect the two "next pointers" */
  Frame *added = new Frame(/*r, */s);
  added->setNextFrame(saved);
  cur->setNextFrame(added);
}

void CircularList::deleteAllFrames(Frame *current) {
  if (current == head || current == nullptr) {
    return;
  } else if (current->getNextFrame() == nullptr) {
    delete current;
  } else {
    deleteAllFrames(current->getNextFrame());
    delete current;
  }
}

Frame *CircularList::getHead() const {
  return head;
}

void CircularList::actualizeAll(SDL_Renderer *r) {
  Frame *saved = head;
  Frame *current = head;
  if (current == nullptr) {
    return;
  }
  current->actualize(r);
  current = current->getNextFrame();
  while (current != saved) {
    current->actualize(r);
    current = current->getNextFrame();
  }
}

void CircularList::deActualizeAll() {
  Frame *saved = head;
  Frame *current = head;
  if (current == nullptr) {
    return;
  }
  current->deActualize();
  current = current->getNextFrame();
  while (current != saved) {
    current->deActualize();
    current = current->getNextFrame();
  }
}


#include "Game.hpp"

#ifdef __linux__
  #include <SDL2/SDL.h>
  #include <SDL2/SDL_ttf.h>
  #include <SDL2/SDL_mixer.h>
#else
  #include <SDL.h>
  #include <SDL_ttf.h>
  #include <SDL_mixer.h>
#endif

#include <iostream>
/*#include <unistd.h>*/

namespace global {
  size_t screen_width = 960;
  size_t screen_height = 540;
}

int main(int argc, char **argv/*void*/) {
  SDL_Window *main_window = nullptr;
  SDL_Renderer *main_renderer = nullptr;
  
  int result = SDL_Init(SDL_INIT_AUDIO|SDL_INIT_EVENTS|SDL_INIT_TIMER);
  if (result != 0) {
    std::cerr << "[FATAL] ERR: Failed to initialize SDL! (Error code " << result << ")" << std::endl;
    std::cout << "[     ] ERR msg: " << SDL_GetError() << std::endl;
    return -1;
  }
  
  result = TTF_Init();
  if (result == (-1)) {
    std::cerr << "[FATAL] ERR: Failed to start SDL TTF addon!" << std::endl;
    return -1;
  }
  
  result = Mix_OpenAudio(44100, AUDIO_S16SYS, 2, 4096);
  result = Mix_Init(MIX_INIT_OGG);
  if (result != MIX_INIT_OGG) {
    std::cerr << "[ ERR ]    Failed to start the SDL Mixer with OGG flags!" << std::endl;
    std::cerr << SDL_GetError() << std::endl;
  }
  
  /*** INIT DISPLAY ***/
  main_window = SDL_CreateWindow("MultiGen Game", 0, 0, global::screen_width,
                                 global::screen_height,
                                 SDL_WINDOW_OPENGL|SDL_WINDOW_RESIZABLE);
  if (main_window == nullptr) {
    std::cerr << "[FATAL] ERR: Failed to create SDL Window!" << std::endl;
    Mix_Quit();
    SDL_Quit();
    return -1;
  }
  
  main_renderer = SDL_CreateRenderer(main_window, -1, SDL_RENDERER_ACCELERATED);
  if (main_renderer == nullptr) {
    std::cerr << "[FATAL] ERR: Failed to start the SDLS Renderer object!" << std::endl;
  }
  SDL_RenderSetLogicalSize(main_renderer, SCREEN_WIDTH, SCREEN_HEIGHT);
  
  WMSL *wmsl = new WMSL(main_renderer, main_window,
                        "res/world-map-3.conf"/*"res/world-map-2.conf"*/);
  result = wmsl->launch();
  delete wmsl;
  SDL_DestroyWindow(main_window);
  Mix_Quit();
  SDL_Quit();
  return result;
}

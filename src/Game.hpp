/*    !!! WARNING!!! THE CODE YOU ARE RUNNING IS (PROBABLY) OUT OF DATE!
 *    Massive changes are being made to this game, especially as of 2019.
 *    The version you are running is (probably) not up-to-date.
 *    Is this a problem? Probably not. But if you're reading this, 
 *    then you're probably one of the people who can read code.
 *    So you should be aware that the major repository (with multiple branches!)
 *    is available at https://bitbucket.org/vjcozzo/multigen_game
 *    Contact `chocorho` with any questions you have.
 */
/*
 *    The MultiGen Game -- an elaborate, customizable 2D strategy game.
 *    Copyright (C) 2019 and GNU GPL'd by chocorho & TrebledJ
 *    Thanks to all code contributors, including but not limited to:
 *      MrMcApple (Java code design & implemenetation)
 *      TrebledJ (C++ design & development)
 *
 *    This program is free software; you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation; either version 2 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License along
 *    with this program; if not, write to the Free Software Foundation, Inc.,
 *    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */
/*
 * "Programs must be written for people to read, and only incidentally for machines to execute."
 *  Harold Abelson, SICP
 * 
 * "What happened to a company making a product and sticking with it? What happened to that? I miss those days."
 *  Bryan Lunduke
 *  
 *  ~THE LONELY LEAD DEVELOPER BLUES~
 *  
 */
#ifndef Game_hpp
#define Game_hpp

#define HALLOWEEN
#define NEW_UI /* treated as a boolean condition */
/* Setting proper header X-coords,
 * that are now all in one place, for simpler one-time
 * modification. */
#ifdef NEW_UI
  #define LR_QUAD_AGE_HDR_X 260
  #define LR_QUAD_LEVEL_HDR_X 320
  #define LR_QUAD_LOCATION_HDR_X 420
  #define LR_QUAD_CHOSEN_HDR_X 520
#else
  #define LR_QUAD_AGE_HDR_X 380
  #define LR_QUAD_LEVEL_HDR_X 440
  #define LR_QUAD_LOCATION_HDR_X 540
  #define LR_QUAD_CHOSEN_HDR_X ((0.9*(global::screen_width-leftPanelBound))) 
#endif

#ifdef _MSC_VER
  #define _CRT_SECURE_NO_WARNINGS
#endif

/* Fundamental poject header files */
#include "game-loop.hpp"
#include "tmb.hpp"

/* Graphics libraries */

#ifdef __linux__
  #include <SDL2/SDL.h>
  #include <SDL2/SDL_ttf.h>
#else
  #include <SDL.h>
  #include <SDL_ttf.h>
#endif

/* Essential system libraries */
#include <string>
#include <vector>

/*#define DEBUG_MSG*/
/*#define CONF_FILE_DEBUG*/

/* I had an idea for a refactor. These Tab classes will lay a
 * foundation for it. -cho, Sept 2019 */
class Tab {
  protected:
    virtual void answerClick(int x, int y, SDL_Texture *t);
};

class MapTab : public Tab {
  void answerClick(int x, int y, SDL_Texture *t) {
    
  }
};

class ArmyTab : public Tab {
};

class ResearchTab : public Tab {
};

class HomefrontTab : public Tab {
};

static constexpr SDL_MessageBoxButtonData cancel_buttons[2] =
{ { SDL_MESSAGEBOX_BUTTON_RETURNKEY_DEFAULT, 1, "YES, quit" },
  { SDL_MESSAGEBOX_BUTTON_ESCAPEKEY_DEFAULT, 2, "NO, restore"} };

class WMSL {
public:
  WMSL(SDL_Renderer *dis, SDL_Window *wind, std::string *name);
  WMSL(SDL_Renderer *dis, SDL_Window *wind, const char *name);
  ~WMSL();
  
  bool getRedraw();
  int getGameState();
  int getThreadState();
  void setThreadState(int n_state);
  void setGameState(int n_state);
  void repaint();
  void deepRepaint();
  bool isPaused();
  void setPaused(bool ex_paused);
  /*    void flipPaused() {
   paused = !paused;
   }*/
  /*    SDL_mutex *get() {
   return ;
   }*/
  
  int launch();
  
protected:
  bool paused = true;
  bool redraw = true;
  bool deep_redraw = true;
  bool thread_status = true;
  int date[3];                //  make date a separate class? -T 
  SDL_mutex *date_mutex;
  /*    int **province_map;*/
  Province **provinces;
  unsigned int num_provinces, enemy_provinces;
  int map_width, map_height;
  visual_code visual_type;
  int result;
  float scale_factor = 1.0f;
  float wait_time;
  Uint32 game_seconds_per_tick; // variable "game rate": number of seconds per tick
  /*    char *time_speed_msg;*/
  std::string date_msg;
  std::string time_speed_msg;
  SDL_Renderer *display;
  SDL_Window *window;
  /*    std::vector<SDL_Window *> *child_windows;*/
  
  SDL_Texture *map_img;
  SDL_Surface *map_img_surf;
  SDL_Texture *internal_map_canvas;
  SDL_Surface *internal_map_surf;
  SDL_Texture *internal_map_img;
  SDL_Texture *lower_right_canvas;
  SDL_Texture *lower_left_canvas;
#ifdef NEW_UI
  SDL_Texture *right_panel_canvas;
#endif
  SDL_Surface *title_screen_surf;
  SDL_Texture *title_screen_text;
  
  SDL_Texture *arrow_imgs[4];
  SDL_Texture *up_img_fill;
  SDL_Texture *up_img_mt;
  SDL_Texture *down_img_fill;
  SDL_Texture *down_img_mt;
  SDL_Texture *make_team_img;
  SDL_Texture *invade_province_img;
  SDL_Texture *invade_settlement_img;
  SDL_Texture *scroll_b;
  
  /* Destination Frame (dframe) struct !!!
   * A general object to store RECTANGULAR coordinates for
   * the destination of an image. This is a single, re-usable object,
   * which saves memory. */
  SDL_Rect *dframe;
  FC_Font *fc_luxirb_small = nullptr;
  FC_Font *fc_luxirb_med = nullptr;
  FC_Font *fc_luxirb_sm = nullptr;
  FC_Font *white_luxirb_med = nullptr;
  FC_Font *white_luxirb_sm = nullptr;
  TTF_Font *font_luxirb_med = nullptr;
  SDL_TimerID ticker_timer;
  
  //  TODO change pointers to vectors, where possible   -T 
  std::vector<Unit *> player_units;
  std::vector<Unit *> retired/*_units*/;
  std::vector<Unit *> opp_units;
  std::vector<Unit *> chosen_ones; /* I *AM* the chosen one */
//  std::vector<unsigned int> chosen_indices;
  std::vector<TacticalMapBattle *> **calendar;
  std::vector<TacticalMapBattle *> **next_year;
  unsigned int last_prov;
  
  /* Next we may need a vector to hold all deceased player units
   * (we may need their trait information after they are deceased).
   * This may (but probably will not) be deemed unnecessary.
   * But in essence, what we're saying is that we need a pool of dead
   * player units, i.e. we need a ...
   * ...
   * ...
   * DEADPOOL. :D
   * e a s t e r  e g g #5 */
  std::vector<Unit *> dead_pool; // holds dead player units... may be unnecessary
  std::vector<TacticalMapBattle *> *pending_battles = nullptr;
  SDL_mutex *battle_control = nullptr;
  SDL_mutex *date_control = nullptr;
  SDL_mutex *prov_control = nullptr;
  SDL_mutex *state_control = nullptr;
  SDL_mutex *thread_control = nullptr;
  SDL_mutex *redraw_control = nullptr;
  SDL_mutex *pause_control = nullptr;
  std::vector<int> *prov_ids;
  std::vector<std::string> *names = nullptr;
  unsigned int unit_disp_ind; // unit display ind, for use when visual_type == 1
  unsigned int retired_disp_ind; // retired display ind, for use when visual_type == 3
  int thread_state;
  int game_state;
  SDL_Color blue_text = {10, 140, 220, 255};
  SDL_Color black_text = {2, 2, 2, 255};
  
  SDL_PixelFormat *rgb_format24 = SDL_AllocFormat(SDL_PIXELFORMAT_RGB24);
  SDL_PixelFormat *rgb_format = SDL_AllocFormat(SDL_PIXELFORMAT_RGB888);
  SDL_PixelFormat *rgba_format = SDL_AllocFormat(SDL_PIXELFORMAT_RGBA8888);
  
  SDL_Surface *mapTextSurf = nullptr;
  SDL_Texture *mapText = nullptr;
  SDL_Surface *armyTextSurf = nullptr;
  SDL_Texture *armyText = nullptr;
  SDL_Surface *researchTextSurf = nullptr;
  SDL_Texture *researchText = nullptr;
  SDL_Surface *homefrontTextSurf = nullptr;
  SDL_Texture *homefrontText = nullptr;
  
  SDL_Texture *espionageText = nullptr;
  SDL_Texture *nameText = nullptr;
  SDL_Texture *logoText = nullptr;
  SDL_Texture *resumeText = nullptr;
  SDL_Texture *saveExitText = nullptr;
  SDL_Texture *exitText = nullptr;
  
private:
  void speed_up();
  void scroll_up();
  void scroll_down();
  
  /* Postcondition: displays a very simple ESC menu as an overlay */
  int show_esc_menu(/*SDL_Texture *map, int code, FC_Font *menu_font*/);
  
//  int fillEdge(int x_val, int y_val, int last_direction, SDL_Texture *bmp,
//               /* SDL_Texture *dest,*/ SDL_Color *color);
  
  void load_names();
  
  void load_map(const char *map_name);  
  bool displayUnitInfo(int x_coord, int y_coord, size_t index);
  
  void spawnUnits();
  
  /* Precondition: none
   * Postcondition: Prompts user for Province choice; sends units to 
   *   destination province; updating unit properties; etc. */
  void invadeProvince();
  
  /* Postcondition: automatically updates province and settlement ownership
   * based on what units are present. 
   * This functionality was sadly abandoned after release version 0.0.1.,
   * as per ECO #1. */
  void intelligentlyUpdateMap();
  
  static int minimal_pause(/*SDL_Thread *thread, */void *arg_void);
  
  /* Helper function used to simply print a nice table,
   * listing all units in a particular vector */
  static void make_table(const std::vector<Unit *> &list, SDL_Renderer *renderer,
                         size_t init_ind, FC_Font *font, SDL_Rect *rframe
                                        /*, WMSL *instance*/);

  /* A function that will generalize the generation of the visual
   * (image, text, etc.) that appears in the
   * lower-right portion of the screen. */
  static int draw_lr_quad(WMSL *instance
  /*SDL_Renderer *render,
   SDL_Texture *canvas, SDL_Texture *map,
   std::vector<Unit *> &player_list, std::vector<Unit *> &chosen_list,
   std::vector<unsigned int> &chosen_ids, unsigned int disp_ind,
   int code, FC_Font *text_font*/);
  
  /* Precondition: general is a non-null Unit ptr.
   * Postcondition: processes the enemy unit's debilitation.
   *   This may include a pop-up announcement, event, etc.
   * TODO fill in this method definition, by consulting a lookup table.  */
  static void processDead(Unit *general);
  
  /* Represents the WMSL AI, which, you'll note, has been shut down
   * and deactivated by default, in versions 0.0.2-0.0.4.
   * A moment of silence for the WMSL AI. :,(   */
  static Uint32 processWmslAI(Uint32 step, void *args);
  
  static void/*Uint32*/ launchAllBattles(void *args);
  
  /* Parmesian package parameter (appreciate the alliteration) */
  static Uint32 processBattles(Uint32 step, void *args);
  
};

#endif /* Game_hpp */

/*
 *    The MultiGen Game -- an elaborate, customizable 2D strategy game.
 *    Copyright (C) 2018 Chozorho
 *    Special thanks to all contributors.
 *
 *    This program is free software; you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation; either version 2 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License along
 *    with this program; if not, write to the Free Software Foundation, Inc.,
 *    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */
#include "tmb.hpp"

#ifdef __linux__
  #include <SDL2/SDL.h>
  #include <SDL2/SDL_image.h>
  #include <SDL2/SDL_ttf.h>
  #include <SDL2/SDL_mixer.h>
#else
  #include <SDL.h>
  #include <SDL_image.h>
  #include <SDL_ttf.h>
  #include <SDL_mixer.h>
#endif

#include <stdio.h>
#include <string>
#include <time.h>
#include <vector>

/* Map Codes
 * (for reference)
 *
  0 = Ocean  
  1 = Plains  
  2 = Forest  
  3 = Hills  
  4 = Ice  
  5 = Sand  
  6 = Mountains  
 */
/* TODO LIST
 * -Logging features, to record extraneous data like damage calculations in files
 * -Enemy AI improvements
 */
/* Tests the TMB in isolation. */
int main(int argc, char **argv) {
  int result;
  
  result = SDL_Init(SDL_INIT_AUDIO|SDL_INIT_EVENTS|SDL_INIT_TIMER);
  if (result != 0) {
    std::cerr << "[FATAL] ERR: Failed to initialize SDL! (Error code " << result << ")" << std::endl;
    std::cout << "[     ] ERR msg: " << SDL_GetError() << std::endl;
    return -1;
  }

  result = TTF_Init();
  if (result == (-1)) {
    std::cerr << "[FATAL] ERR: Failed to start SDL TTF addon!" << std::endl;
    return -1;
  }

  result = Mix_OpenAudio(44100, AUDIO_S16SYS, 2, 4096);
  result = Mix_Init(MIX_INIT_OGG);
  if (result != MIX_INIT_OGG) {
    std::cerr << "[ ERR ]    Failed to start the SDL Mixer with OGG flags!" << std::endl;
    std::cerr << SDL_GetError() << std::endl;
  }
  
  /*** INIT DISPLAY ***/
//  SDL_Window *main_window = SDL_CreateWindow("MultiGen Game", 0, 0, TMB_SCREEN_WIDTH, TMB_SCREEN_HEIGHT, SDL_WINDOW_OPENGL);
//  if (main_window == nullptr) {
//    std::cerr << "[FATAL] ERR: Failed to create SDL Window!" << std::endl;
//    return -1;
//  }
//  
//  SDL_Renderer *main_renderer = SDL_CreateRenderer(main_window, -1, SDL_RENDERER_ACCELERATED);
//  if (main_renderer == nullptr) {
//    std::cerr << "[FATAL] ERR: Failed to start the SDLS Renderer object!" << std::endl;
//  }
//  SDL_RenderSetLogicalSize(main_renderer, TMB_SCREEN_WIDTH, TMB_SCREEN_HEIGHT);
  
  std::vector<Unit *> side_one/**side_one = new std::vector<Unit *>()*/;
  std::vector<Unit *> side_two/**side_two = new std::vector<Unit *>()*/;
  side_one.reserve(8);
  side_two.reserve(8);
  Unit *testArcher = new Unit(ARCHER, 5, 7, 0, "Tom");
  testArcher->setSprite("res/Archer.png");
  Potion *testPt1 = new Potion(6);
  testArcher->addItem(testPt1);
  side_one.push_back(testArcher);
  
  Unit *testArcher2 = new Unit(ARCHER, 4, 3, 0, "Minimax");
  testArcher2->setSprite("res/Archer.png");
  Potion *testPt2 = new Potion(8);
  Potion *testPt3 = new Potion(7);
  testArcher2->addItem(testPt2);
  testArcher2->addItem(testPt3);
  side_one.push_back(testArcher2);
  
  Unit *testArcher3 = new Unit(ARCHER, 6, 3, 0, "Bert");
  testArcher3->setSprite("res/Archer.png");
  Potion *testPt4 = new Potion(4);
  Potion *testPt5 = new Potion(5);
  Potion *testPt6 = new Potion(1);
  Potion *testPt7 = new Potion(3);
  testArcher3->addItem(testPt4);
  testArcher3->addItem(testPt5);
  testArcher3->addItem(testPt6);
  testArcher3->addItem(testPt7);
  side_one.push_back(testArcher3);
  
  std::stringstream map_name;
  int map_num = generator() % NUM_TMB_MAP_CHOICES;
/*  std::cout<<"Verbosity identified as : "<<verbose<<std::endl;*/
  std::cout<<"Map code randomly generated : "<<map_num<<std::endl;
  map_name << "res/maps/unsorted/map" << map_num << ".map";
  
  if (argc == 2) {
    TacticalMapBattle *game = new RoutTMB
      (map_name.str().c_str(), "res/terrains.conf"/*"res/halloween-terrains.conf"*/,
       side_one, side_two, 0);
    result = game->launch();
    delete game;
//    SDL_DestroyWindow(main_window);
    Mix_Quit();
    return result;
  } else {
    std::cerr<<"Usage: ./oo-tmb [VERBOSENESS]"<<std::endl;
    std::cerr<<"The parameter should be an integer, 0 to request quiet output"\
        " and nonzero for verbose output."<<std::endl<<std::endl;
    return 1;
  }

//  struct timespec ctt;
//  clock_gettime(CLOCK_REALTIME, &ctt);
//  long nano = ctt.tv_nsec;
//  std::mt19937_64 generator(nano);
}

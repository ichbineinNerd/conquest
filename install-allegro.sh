#!/usr/bin/env bash
#
#
#set -o errexit

# Update packages and install dependencies (only need to run this once!)
echo "deb http://ftp.us.debian.org/debian lenny main" >> /etc/apt/sources.list
apt update
apt install build-essential git git-core cmake cmake-curses-gui xorg-dev libgl1-mesa-dev libglu-dev
apt install libpng-dev libcurl4-openssl-dev libfreetype6-dev libjpeg-dev libvorbis-dev libopenal-dev libphysfs-dev \
	libgtk2.0-dev libasound-dev libpulse-dev libflac-dev libdumb1-dev

# Clone the repository (only once!)
git clone https://github.com/liballeg/allegro5.git
cd allegro5

# This is our "build directory"...
# If you want to re-build, either delete this and recreate it,
# or make a new directory in place of it.
mkdir linux-build; cd linux-build

# These steps do the actual compiling (run as many times as you need):
cmake -DCMAKE_INSTALL_PREFIX=/usr/ ../
time make
make install
ldconfig

